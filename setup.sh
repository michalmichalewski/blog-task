#!/bin/bash

docker exec -i php php -d memory_limit=-1 /usr/bin/composer install --dev
docker exec -i php php bin/console doctrine:database:drop --force
docker exec -i php php bin/console doctrine:database:create
docker exec -i php php bin/console doctrine:schema:update --force
docker exec -i php php bin/console doctrine:fixtures:load