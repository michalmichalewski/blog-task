DEV_SERVER=192.168.0.113
PROJECT_DIR=/var/www
ssh:
	ssh root@$(DEV_SERVER)
ls:
	echo "restart"
restart:
	ssh root@$(DEV_SERVER) 'cd $(PROJECT_DIR) && docker-compose down && docker-compose up -d'
stop:
	ssh root@$(DEV_SERVER) 'cd $(PROJECT_DIR) && docker-compose down'
start:
	ssh root@$(DEV_SERVER) 'cd $(PROJECT_DIR) && docker-compose up -d'
logs:
	ssh root@$(DEV_SERVER) 'cd $(PROJECT_DIR) && docker logs php --follow'
shell:
	ssh root@$(DEV_SERVER) 'cd $(PROJECT_DIR) && docker exec -it php bash'
console:
	ssh root@$(DEV_SERVER) 'docker exec -i php php bin/console $(cmd)'
doctrine-migration-dump:
	ssh root@$(DEV_SERVER) 'docker exec -i php php bin/console doctrine:schema:update --dump-sql'
doctrine-exec-diff:
	ssh root@$(DEV_SERVER) 'docker exec -i php php bin/console doctrine:schema:update --force'
composer-install:
	ssh root@$(DEV_SERVER) 'docker exec -i php composer require $(cmd)'
	rsync -avr root@$(DEV_SERVER):$(PROJECT_DIR)/symfony/composer.lock symfony/composer.lock
	rsync -avr root@$(DEV_SERVER):$(PROJECT_DIR)/symfony/composer.json symfony/composer.json
	rsync -avr root@$(DEV_SERVER):$(PROJECT_DIR)/symfony/vendor/* symfony/vendor
migration:
	ssh root@$(DEV_SERVER) 'docker exec -i php php bin/console make:migration'
	rsync -avr root@$(DEV_SERVER):$(PROJECT_DIR)/symfony/src/Migrations/* symfony/src/Migrations
	git add symfony/src/Migrations/*
validator:
	ssh root@$(DEV_SERVER) 'docker exec -i php php bin/console make:validator'
	rsync -avr root@$(DEV_SERVER):$(PROJECT_DIR)/symfony/src/Validator/* symfony/src/Validator
migrate:
	ssh root@$(DEV_SERVER) 'docker exec -i php php bin/console doctrine:migrations:migrate'