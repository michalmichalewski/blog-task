<?php

namespace App\Twig;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

final class HighlightExtension extends AbstractExtension
{

    protected ?string $searchTerm;

    /**
     * @var mixed
     */
    private $approximateSearchTerm;

    private LoggerInterface $logger;

    public function __construct(
        RequestStack $requestStack,
        LoggerInterface $logger
    )
    {
        $this->logger = $logger;

        if($requestStack->getCurrentRequest())  {

            $this->searchTerm = $requestStack->getCurrentRequest()->query->get('searchTerm');
            $this->approximateSearchTerm = $requestStack->getCurrentRequest()->query->get('approximateSearchTerm');
        }
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('highlightSearchTerm', [$this, 'highlightFromRequest'])
        ];
    }

    private function highlight(string $text): string
    {
        return '<span class="highlight">'.$text.'</span>';
    }

    public function highlightFromRequest(string $text, $approximation = 80): string
    {
        if(($this->searchTerm === null || $this->approximateSearchTerm === null) === false) {
            return $text;
        }

        if($this->searchTerm) {
            $words = explode(' ', $text);

            foreach ($words as &$word) {

                if(strpos($word, $this->searchTerm) !== false) {
                    $word = $this->highlight($word);
                }
            }

            return implode(' ', $words);
        }


        if($this->approximateSearchTerm) {
            $words = explode(' ', $text);

            foreach ($words as &$word) {
                $percent = 0;
                similar_text($word, $this->approximateSearchTerm, $percent);

                $this->logger->debug('Percent ',['percent' => $percent]);

                if($percent >= $approximation) {
                    $word = $this->highlight($word);
                }
            }

            return implode(' ', $words);
        }

        return $text;
    }
}
