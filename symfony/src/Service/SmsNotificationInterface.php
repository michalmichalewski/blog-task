<?php

namespace App\Service;


interface SmsNotificationInterface
{
    public function send($message, $phoneNumber): \Symfony\Contracts\HttpClient\ResponseInterface;
}