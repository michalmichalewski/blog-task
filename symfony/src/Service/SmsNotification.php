<?php
namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SmsNotification implements SmsNotificationInterface
{
    protected HttpClientInterface $client;
    protected LoggerInterface $logger;

    public function __construct(HttpClientInterface $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    public function send($message, $phoneNumber): \Symfony\Contracts\HttpClient\ResponseInterface
    {
        $response = $this->client->request('POST', 'http://178.219.17.198:8723/cgi-bin/sms.sh', [
            'body' => [
                'msg' => $message,
                'msisdn' => $phoneNumber,
                'action' => 'send',
                'name' => 'submit'
            ]
        ]);

        $this->logger->debug($response->getContent());

        return $response;
    }
}