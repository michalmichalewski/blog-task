<?php

namespace App\Entity;

use App\Repository\PostRepository;
use Doctrine\Common\Collections\AbstractLazyCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
class Post implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected string $title;
    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    protected string $content;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="post", fetch="EXTRA_LAZY")
     */
    protected AbstractLazyCollection $comments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     */
    protected $author;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected \DateTime $createdDate;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected \DateTime $updatedDate;

    public function __construct()
    {
        $this->updatedDate = new \DateTime();
        $this->setCreatedAt();
    }

    protected function setCreatedAt()
    {
        if(empty($this->createdDate)) {
            $this->createdDate = new \DateTime();
        }
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getComments(): AbstractLazyCollection
    {
        return $this->comments;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate(\DateTime $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDate(): \DateTime
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate(\DateTime $updatedDate): void
    {
        $this->updatedDate = $updatedDate;
    }

    public function addComment(string $content, string $ip)
    {
        $comment = new Comment($content, $ip, $this);
        $this->comments->add($comment);
    }

    public function jsonSerialize(): array
    {
        return [
            'id'        => $this->getId(),
            'title'     => $this->getTitle(),
            'content'   => $this->getContent(),
            'createdDate' => $this->createdDate
        ];
    }

    public function __toString()
    {
        return (string)$this->getId();
    }


}
