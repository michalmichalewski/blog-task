<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Vote
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\VoteRepository")
 * @ORM\Table()
 */
class Vote
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="datetime")
     */
    protected ?\DateTime $createdDate;
    /**
     * @ORM\Column(type="datetime")
     */
    protected ?\Datetime $dueDate;
    /**
     * @ORM\Column(type="string")
     */
    protected string $question;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vote\Result", mappedBy="vote", fetch="EXTRA_LAZY")
     */
    protected $results;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected bool $isActivated;

    public function __construct()
    {
        $this->setCreatedDate();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param mixed $createdDate
     */
    private function setCreatedDate(): void
    {
        if(!isset($this->createdDate)) {
            $this->createdDate = new \DateTime();
        }

    }

    /**
     * @return mixed
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @param mixed $dueDate
     */
    public function setDueDate($dueDate): void
    {
        $this->dueDate = $dueDate;
    }

    /**
     * @return mixed
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param mixed $question
     */
    public function setQuestion($question): void
    {
        $this->question = $question;
    }

    /**
     * @return bool
     */
    public function isActivated(): bool
    {
        return $this->isActivated;
    }

    /**
     * @param bool $isActivated
     */
    public function setIsActivated(bool $isActivated): void
    {
        $this->isActivated = $isActivated;
    }

    public function isDisabled()
    {
        return $this->isActivated === true;
    }
}