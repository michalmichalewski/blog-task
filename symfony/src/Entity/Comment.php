<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 * @ORM\Table()
 */
class Comment implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private string $content;
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected string $ip;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdDate;

    /**
     * @var Post
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="comments")
     */
    protected ?Post $post;


    public function __construct()
    {
        $this->setCreatedDate();
    }

    private function setCreatedDate(): void
    {
        if(empty($this->createdDate)) {
            $this->createdDate = new \DateTime();
        }
    }

    public function id(): int
    {
        return $this->id;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function ip()
    {
        return $this->ip;
    }

    public function getId()
    {
        return $this->id();
    }

    public function post(): Post
    {
        return $this->post;
    }

    public function createdDate()
    {
        return $this->createdDate;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip): void
    {
        $this->ip = $ip;
    }

    /**
     * @param Post $post
     */
    public function setPost(?Post $post): void
    {
        $this->post = $post;
    }

    public function jsonSerialize()
    {
        return [
            'id'        => $this->id(),
            'content'   => $this->content(),
            'createDate'=> $this->createdDate()->format('Y-m-d H:i:s'),
            'ip'        => $this->ip()
        ];
    }
}
