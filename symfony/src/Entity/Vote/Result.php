<?php

namespace App\Entity\Vote;

use App\Entity\User;
use App\Entity\Vote;
use Doctrine\DBAL\Schema\Column;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Result
 * @package App\Entity\Vote
 * @ORM\Table(name="user_votes",uniqueConstraints={@ORM\UniqueConstraint(name="search_idx", columns={"vote_id", "user_id"})})
 * @ORM\Entity()
 */
class Result
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vote",inversedBy="results" );
     */
    protected $vote;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false, fieldName="user_id")
     */
    protected $user;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $result;

    public function __construct(User $user, Vote $vote, bool $result)
    {
        $this->user = $user;
        $this->vote = $vote;
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return Vote
     */
    public function getVote(): Vote
    {
        return $this->vote;
    }

    /**
     * @param Vote $vote
     */
    public function setVote(Vote $vote): void
    {
        $this->vote = $vote;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return bool
     */
    public function isResult(): bool
    {
        return $this->result;
    }

    /**
     * @param bool $result
     */
    public function setResult(bool $result): void
    {
        $this->result = $result;
    }



}