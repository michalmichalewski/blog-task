<?php

namespace App;

use App\Entity\Post;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PostDecorator implements \JsonSerializable
{
    protected Post $post;
    protected UrlGeneratorInterface $generator;

    public function __construct(Post $post, UrlGeneratorInterface $generator)
    {
        $this->post = $post;
        $this->generator = $generator;
    }

    public function jsonSerialize()
    {

        $data = $this->post->jsonSerialize();
        $data['url'] = $this->generator->getContext()->getScheme().'://'.
            $this->generator->getContext()->getHost().
            $this->generator->generate('user_post_read', ['id' => $this->post->getId()]);

        return $data;
    }


}