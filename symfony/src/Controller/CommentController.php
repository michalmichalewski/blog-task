<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CommentController
 * @package App\Controller
 */
class CommentController extends AbstractController
{

    protected $commentRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->commentRepository = $entityManager->getRepository(Comment::class);
    }

    /**
     * @Route(name="get_post_comments", methods={"GET"}, path="/app/post/{id}/comment")
     */
    public function index(Post $post)
    {
        return $this->json($this->commentRepository->findBy([
            'post' => $post->getId()
        ], ['id' => 'desc']));
    }

    /**
     * @Route(name="create_comment", methods={"POST"}, path="/post/{id}/comment")
     */
    public function create(Request $request, EntityManagerInterface $entityManager, Post $post)
    {
        $form = $this->createForm(CommentType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            /** @var Comment $comment */
            $comment = $form->getData();
            $comment->setPost($post);
            $comment->setIp($request->getClientIp());

            $entityManager->persist($comment);
            $entityManager->flush();

            $this->get("security.csrf.token_manager")->refreshToken("form_intention");

            return $this->json($comment);
        } else {
            return $this->json([
                'message' => 'Comment already added'
            ],400);
        }
    }
}