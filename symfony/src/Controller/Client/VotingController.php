<?php

namespace App\Controller\Client;

use App\Entity\Vote;
use App\Form\UserVoteType;
use App\Repository\VoteRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VotingController
 * @package App\Controller\Client
 * @Route ("/app/voting")
 */
class VotingController extends AbstractController
{
    protected $votingRepository;

    /**
     * @Route(name="user_voting", path="/")
     * @param EntityManager $entityManager
     */
    public function index(EntityManagerInterface $entityManager)
    {
        /** @var VoteRepository votingRepository */
        $this->votingRepository = $entityManager->getRepository(Vote::class);
        $result = $this->votingRepository->findAllNotVotedByUser($this->getUser());

        return $this->render('vote/client_vote.html.twig', [
            'votes' => $result,
            'title' => 'Głosowania'
        ]);
    }

    private function checkResultOfVote(Vote $vote)
    {
        $result = $this->getDoctrine()->getRepository(Vote\Result::class)
            ->findBy([
                'user' => $this->getUser(),
                'vote' => $vote
            ]);

        return $result !== null;
    }

    /**
     * @Route(path="/{id}", name="take_a_vote")
     */
    public function takeAVote(
        Vote $vote,
        Request $request,
        EntityManagerInterface  $entityManager,
        LoggerInterface $logger
    )
    {

        if($this->checkResultOfVote($vote)) {
            $this->addFlash('danger', 'Głos został już oddany');
            return $this->redirectToRoute('user_voting');
        }

        $form = $this->createForm(UserVoteType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $yes = $form->all()['yes']->isClicked();
            $no = $form->all()['no']->isClicked();

            try  {
                $result = new Vote\Result($this->getUser(), $vote, $yes);
                $entityManager->persist($result);
                $entityManager->flush();

                $this->addFlash('success', 'Dziękujemy za oddanie głosu');
                $this->redirectToRoute('user_voting');
            }
            catch (\Exception $exception) {
                $logger->alert($exception->getMessage());
            }

        }

        return $this->render('vote/take_a_vote.html.twig', [
            'form' => $form->createView(),
            'vote' => $vote
        ]);
    }
}