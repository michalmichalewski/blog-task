<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Form\CommentType;
use App\Form\PostSearchType;
use App\Form\SearchType;
use App\PostDecorator;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/app/forum")
 */
class DefaultController extends AbstractController
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="default")
     */
    public function index(): Response
    {
        $posts = $this->entityManager->getRepository(Post::class)->findAll();

        return $this->render('frontend/index.html.twig', [
            'posts' => $posts,
            'searchForm' => $this->createForm(SearchType::class)->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/search", methods={"AJAX", "GET"}, name="search_in_blog")
     */
    public function search(Request $request, UrlGeneratorInterface $generator): Response
    {
        $queryParam = $request->query->get('searchTerm');

        /** @var PostRepository $postRepository */
        $postRepository = $this->entityManager->getRepository(Post::class);

        $result = $postRepository->search($queryParam);

        return $this->json( array_map(fn(Post $post) => new PostDecorator($post,$generator), $result) );
    }

    /**
     * @param Request $request
     * @param Post $post
     * @return Response
     * @Route(path="/read/{id}", name="user_post_read", methods={"POST", "GET"})
     */
    public function read(Request $request, Post $post): Response
    {
        $form = $this->createCommentForm($request, $post);

        $searchForm = $this->createForm(PostSearchType::class, [], [
            'method' => 'GET'
        ]);

        return $this->render('frontend/read.html.twig', [
            'post' => $post,
            'commentForm' => $form->createView(),
            'postSearchForm' => $searchForm->createView()
        ]);
    }

    private function createCommentForm(Request $request, Post $post): FormInterface
    {
        $form = $this->createForm(CommentType::class, new Comment(), [
            'attr' => [
                'id' => 'comment_form'
            ]
        ]);

        return $form;
    }
}
