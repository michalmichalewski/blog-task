<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Vote;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\User\UserInterface;

class VoteRepository extends EntityRepository
{
    public function findAllNotVotedByUser(UserInterface $user)
    {
        $result =  $this->getEntityManager()->getConnection()->executeQuery('
            SELECT id FROM vote 
                WHERE id not in ( SELECT vote_id FROM `user_votes` WHERE user_id = :user_id)
                    AND is_activated = 1 
                    AND due_date >= :date
        ',[
            'user_id' => $user->getId(),
            'date' => (new \DateTime())->format('Y-m-d H:i:s')
        ])->fetchAllAssociative();

        return $this->createQueryBuilder('v')->where('v.id in (:ids)')->setParameter('ids', $result)
            ->getQuery()->getResult();
    }
}