<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    protected $passwordEncoder;
    protected $generator;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->generator = new LoremIpsum();
    }

    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setUsername('admin');
        $user->setPassword($this->passwordEncoder->encodePassword($user,'1234'));

        $manager->persist($user);
        $manager->flush();

        for($i = 0; $i < 100; $i++) {
            $post = new Post();
            $post->setTitle($this->generator->words(5));
            $post->setContent($this->generator->sentences(4));
            $post->setAuthor($user);

            $manager->persist($post);

            if($i % 10 === 0) {
                $manager->flush();
            }
        }

        $manager->flush();
    }
}
