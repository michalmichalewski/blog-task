<?php

namespace App\Form;

use App\Entity\Vote;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('createdDate', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Data utworzenia',
                'disabled' => true,
                'data' => new \DateTime()
            ])
            ->add('dueDate', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Termin końcowy głosowania'
            ])
            ->add('question', TextType::class, [
                'label' => 'Pytanie'
            ])
            ->add('isActivated', CheckboxType::class, [
                'label' => 'Czy aktywny',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vote::class,
        ]);
    }
}
