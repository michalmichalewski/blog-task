<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PostSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('approximateSearchTerm', TextType::class, [
            'attr' => [
                'placeholder' => 'Search in post'
            ]
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}