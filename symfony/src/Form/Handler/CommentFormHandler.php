<?php


namespace App\Form\Handler;


use App\Entity\Post;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class CommentFormHandler
{
    protected FormInterface $form;
    protected Post $post;

    public function __construct(FormInterface $form, Post $post)
    {
        $this->form = $form;
        $this->post = $post;
    }

    public function handleRequest(Request $request)
    {
        $this->form->handleRequest($request);

        if($this->form->isValid() && $this->form->isSubmitted()) {

        }
    }
}